module.exports = {
	content: ["./src/**/*.{html,js,css}"],
	theme: {
		extend: {
			fontFamily: {
				"outfit": ["Outfit"]
			},
			colors: {
				"npc-soft-blue": "hsl(215, 51%, 70%)",
				"npc-cyan": "hsl(178, 100%, 50%)",
				"npc-cyan-hs": "hsl(178, 100%, 50%, 0.5)",
				"npc-very-dark-blue-mbg": "hsl(217, 54%, 11%)",
				"npc-very-dark-blue-cgb": "hsl(216, 50%, 16%)",
				"npc-very-dark-blue-l": "hsl(215, 32%, 27%)",
				"npc-white": "hsl(0, 0%, 100%)"
			}
		},
	},
	plugins: [],
}
